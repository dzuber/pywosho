Funktionen
==========

Definition
----------

Funktionen werden mit dem Schluesselwort ``def`` definiert::

  >>> def fib(n):    # write Fibonacci series up to n
  ...     """Print a Fibonacci series up to n."""
  ...     a, b = 0, 1
  ...     while a < n:
  ...         print a,
  ...         a, b = b, a+b
  ...
  >>> # Now call the function we just defined:
  ... fib(2000)
  0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597
  >>> def print_stuff():
  ...     print "Stuff"
  >>> print_stuff()
  Stuff

Auf def folgt der name der Funktion. In Klammern stehen die moeglichen Argumente.
Hat die Funktion keine Argumente, sind die Klammern leer. Mehrere Argumente werden mit
Kommata getrennt.

Rueckgabewerte
--------------

Eine Funktion **kann** Werte zurueckgeben, wenn sie es will. Dazu wird das Schluesselwort ``return`` verwendet.
Der Typ ist egal, d.h. eine Funktion kann mehrere Typen zurueckgeben::

  >>> def double_the_fun(fun):
  ...     return fun*2
  >>> double_the_fun(20):
  40
  >>> f = double_the_fun('Fun')
  >>> f
  'FunFun'
  >>> def filter_small(x):
  ...     if x < -10:
  ...         return x
  ...     if x > 10:
  ...         return x
  ...     print 'TO SMALL!'
  >>> r = filter_small(20)
  >>> r
  20
  >>> r = filter_small(-11)
  >>> r
  -11
  >>> r = filter_small(4)
  TOO SMALL
  >>> r
  None

``return`` beendet die Funktion auf der Stelle.

Defaultargumente
----------------

Fuer die Argumente einer Funktion koennen Standardwerte vergeben werden. Damit wird das Argument optional::

  >>> def pay(total, tip=0):
  ...     if not tip:
  ...         print "Thanks, but no thanks!"
  ...     print "You paid %s$ and gave me a %s$ tip" % (total, tip)

Defaultargumente muessen immer nach positionellen Argumenten stehen. Allerdings koennen
Argumente auch per Name in beliebieger Reihenfolge angegeben werden::

  >>> def parrot(voltage, state='a stiff', action='voom', type='Norwegian Blue'):
  ...     print "-- This parrot wouldn't", action,
  ...     print "if you put", voltage, "volts through it."
  ...     print "-- Lovely plumage, the", type
  ...     print "-- It's", state, "!"
  >>> parrot(1000)                                          # 1 positional argument
  >>> parrot(voltage=1000)                                  # 1 keyword argument
  >>> parrot(voltage=1000000, action='VOOOOOM')             # 2 keyword arguments
  >>> parrot(action='VOOOOOM', voltage=1000000)             # 2 keyword arguments
  >>> parrot('a million', 'bereft of life', 'jump')         # 3 positional arguments
  >>> parrot('a thousand', state='pushing up the daisies')  # 1 positional, 1 keyword

Folgende Aufrufe waeren falsch::

  >>> parrot()                     # required argument missing
  >>> parrot(voltage=5.0, 'dead')  # non-keyword argument after a keyword argument
  >>> parrot(110, voltage=220)     # duplicate value for the same argument
  >>> parrot(actor='John Cleese')  # unknown keyword argument

Beliebige Argumentenanzahl
--------------------------

Die ``print`` Funktion in Python kann beliebig viele Argumente aufnehmen. Dieses Verhalten
kann man einfach rekonstruieren. ``prices`` verhaelt sich wie eine Liste::

  >>> def make_bill(customer, *prices):
  ...     print "The bill for %s:" % customer
  ...     total = 0
  ...     for p in prices:
  ...         print "%s$ +" % p
  ...         total += p
  ...     print "-"*20
  ...     print 'TOTAL: %s' % total
  >>> make_bill('Mr. Anderson', 1.5, 20.4, 19.99, 20)
  The bill for Mr. Anderson:
  1.5$ +
  20.4$ +
  19.99$ +
  20$ +
  --------------------
  TOTAL: 61.89
  >>> make_bill('Uncle Scrooge')
  The bill for Uncle Scrooge:
  --------------------
  TOTAL: 0

Man kann auch die Anzahl von sogenannten Keywordargumenten beliebig machen. ``keywords`` verhaelt sich hier wie ein ``dictionary``::

  >>> def cheeseshop(kind, *arguments, **keywords):
  ...     print "-- Do you have any", kind, "?"
  ...     print "-- I'm sorry, we're all out of", kind
  ...     for arg in arguments:
  ...         print arg
  ...     print "-" * 40
  ...     keys = sorted(keywords.keys())
  ...     for kw in keys:
  ...         print kw, ":", keywords[kw]
  >>> cheeseshop("Limburger", "It's very runny, sir.",
                 "It's really very, VERY runny, sir.",
                 shopkeeper='Michael Palin',
                 client="John Cleese",
                 sketch="Cheese Shop Sketch")
  -- Do you have any Limburger ?
  -- I'm sorry, we're all out of Limburger
  It's very runny, sir.
  It's really very, VERY runny, sir.
  ----------------------------------------
  client : John Cleese
  shopkeeper : Michael Palin
  sketch : Cheese Shop Sketch

Manchmal liegen einem die Argumente schon als Liste vor::

  >>> args = ['Limburger', "It's very runny, sir.", "It's really very, VERY runny, sir."]
  >>> kwargs = {'shopkeeper':'Michael Palin',
               'client':"John Cleese",
               'sketch':"Cheese Shop Sketch"}
  >>> cheeseshop(*args, **kwargs)
