Errors
======

In Python kann es waehrend der Laufzeit zu vielen Fehlern kommen::

  >>> d = {'a': 1}
  >>> d['b']
  
  Traceback (most recent call last):
    File "<pyshell#2>", line 1, in <module>
      d['b']
  KeyError: 'b'

Oftmals erhaelt eine Funktion Input von aussen, dem man nicht trauen kann::

  >>> def add(x, y):
  	r = x + y
  	return r
  
  >>> add (1, 'saf')
  
  Traceback (most recent call last):
    File "<pyshell#25>", line 1, in <module>
      add (1, 'saf')
    File "<pyshell#24>", line 2, in add
      r = x + y
  TypeError: unsupported operand type(s) for +: 'int' and 'str'

In diesem Fall kann man in Python per ``try`` und ``except`` Ausnahmen abfangen::

  >>> def safer_add(x, y):
  ...     try:
  ...         r = x + y
  ...     except TypeError:
  ...         print 'Cannot add %s and %s'
  ...         r = None
  ...     return r

Tritt ein Fehler auf, wird der Programmablauf unterbochen. Wird der Fehler in einem ``except``-Block abgefangen, laeuft das Programm danach weiter.

Anmerkungen:

  1. Es sind mehrere ``except``-Bloecke hintereinander moeglich, sofern sie andere Fehler abfangen.
  2. Der Fehlertyp ist nur Optional. Wird er weggelassen werden alle Arten von Fehlern abgefangen.
  3. Man kann mit einem ``except``-Block mehrere Fehlertypen abfangen, indem man sie wie ein Tupel notiert: ``(ValueError, KeyError, RuntimeError)``

Es gibt noch 2 optionale Bloecke, den ``else``- und den ``finally``-Block.
``else`` folgt auf alle ``except``-Bloecke und wird nur ausgefuehrt, wenn kein Fehler passiert ist. ``finally`` wird **immer** ausgefuehrt.

Man kann Fehler auch bewusst weiterleiten (``throw`` in Java). Dazu benutzt man das Schluesselwort ``raise``::

  >>> def check(answer):
      if answer != 42:
          raise ValueError('The answer is not correct!')

  >>> check(15)
  
  Traceback (most recent call last):
    File "<pyshell#38>", line 1, in <module>
      check(15)
    File "<pyshell#37>", line 3, in check
      raise ValueError('The answer is not correct!')
  ValueError: The answer is not correct!

Alle Errors die bisher geziegt wurden, sind eingebaute Fehler in Python und stehen immer zur Verfuegung. Im Grunde sind es Klassen, die ``Exception`` erben. Es ist also moeglich eigene Fehler zu definieren::

  >>> class MyWarning(Exception):
  	pass
  
  >>> try:
  ...     raise MyWarning('Achtung vor dem Hund')
  ... except MyWarning, e:
  ...     print 'Catched: %s' % e
  ... finally:
  ...     print 'Back to normal!'

  Catched: Achtung vor dem Hund
  Back to normal!

``e`` ist in diesem Fall eine Referenz auf die Instanz von MyWarning. Mann kann ``e`` auch anders nennen.
