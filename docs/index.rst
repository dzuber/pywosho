.. Python Workshop documentation master file, created by
   sphinx-quickstart on Mon Apr 21 23:05:30 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Python 101
===========================================

Herzlich willkommen zum Python Workshop! Auf der heutigen Tageskarte steht:

.. toctree::
   :maxdepth: 2
   :numbered:

   intro
   simpledatatypes
   flow
   functions
   classes
   exceptions
   modules

..
   Exceptions
       Try except
       Raise
       User defined Exceptions
       Else Finally
   Modules
       Packages
       Importing



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

