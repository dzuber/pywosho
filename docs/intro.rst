Kurzes Vorgeplaenkel
====================

| Python ist eine High-Level-Programmiersprache mit Fokus auf **Lesbarkeit**.
| Sie ist **dynamisch** und **objektorientiert**. Python bietet die Moeglichkeit andere Sprachen wie **C** einzubetten oder kann als Skriptsprache fuer andere Programme verwender werden (Blender, Cinema 4D, GIMP, Maya, OpenOffice, Nuke, 3ds Max etc).
| Python wird ueblicherweise **interpretiert**, d.h. der Quellcode wird zur Laufzeit eingelesen, analysiert und ausgefuehrt. Andere Sprachen werden vorher *kompilliert*. Deswegen kann Python auch fuer Scripte oder **interaktiv** verwendet werde.
| 
| Python erschien schon 1991 (Java ist z.B. vier Jahre spaeter erschienen). Trotzdem ist Python sehr modern und hat mittlerweile eine betraechtliche Community. Der Name geht nicht auf die Schlangengattung zurueck, sondern auf diese Jungs hier:
| 

.. figure:: images/montypython.jpg
   :width: 500px
   :align: center
   :alt: Monty Python

   "I don't think there's a punch-line scheduled, is there?"

Ihr koennt Python ueber mehrere Arten interaktiv benutzen:

  1. In die Kommandozeile einfach mal ``python`` eingeben und loslegen.
  2. Maya starten und den Scripteditor benutzen.
  3. Die Software ``Idle`` benutzen.

Es lohnt, sich schon mal an den Maya Script Editor zu gewoehnen!

Was tun?
--------

Zuerst einmal **Maya** starten und den **Scripteditor** oeffnen:

  .. figure:: images/scripteditor.png

     Scripteditor mit dem Button unten links oeffnen und ``Show Stack Trace`` aktivieren!

Die History kann man erstmal loeschen:

  .. figure:: images/clearhistory.png

     **Rechte Maustaste** gedrueckt halten und ziehen.
     ``Clear All`` loescht auch alles, was ihr unter der History reingeschrieben habt!

Schreibt nun in den unteren Bereich::

  import this

und drueckt die Entertaste (die am Ziffernblock, nicht die Return Taste in der Mitte). In der History solltet ihr nun folgendes sehen:

  |spoiler|
  ::

    import this
    The Zen of Python, by Tim Peters
    
    Beautiful is better than ugly.
    Explicit is better than implicit.
    Simple is better than complex.
    Complex is better than complicated.
    Flat is better than nested.
    Sparse is better than dense.
    Readability counts.
    Special cases aren't special enough to break the rules.
    Although practicality beats purity.
    Errors should never pass silently.
    Unless explicitly silenced.
    In the face of ambiguity, refuse the temptation to guess.
    There should be one-- and preferably only one --obvious way to do it.
    Although that way may not be obvious at first unless you're Dutch.
    Now is better than never.
    Although never is often better than *right* now.
    If the implementation is hard to explain, it's a bad idea.
    If the implementation is easy to explain, it may be a good idea.
    Namespaces are one honking great idea -- let's do more of those!

  |spoilerend|

Genug mit den Faxen, jetzt gehts mit etwas Gescheitem weiter!
