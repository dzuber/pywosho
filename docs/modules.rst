Module
======

Module gliedern Pythoncode in verschiedene Namensraeume. Ein Modul kann ganz einfach anlegen indem man eine Datei mit der Endung ``.py`` erstellt und dort Pythoncode reinschreibt::

  # Datei mylittlemod.py
  x = 10
  print x
  def foo():
      print x

Import
------

Module werden mit dem Schluesselwort ``import`` geladen. Dabei wird der Code in dem Module ausgefuehrt::

  >>> import mylittlemod
  10
  >>> mylittlemod.x = 20
  >>> mylittlemod.foo()
  20

Wichtig ist, dass mylittlemod.py an einer Stelle liegen muss, andem es auch gefunden werden kann.
Das eingebaute Modul ``sys`` kann immer importiert werden. Mit ihm kann man herausfinden, wo Python nach Modulen sucht::

  >>> import sys
  >>> for p in sys.path:
  ...     print p

Mit ``sys.path.append('C:/path/to/mymod/')`` koennen Ordner waehrend der Laufzeit hinzugefuegt werden. Um einen Ordner dauerhaft hinzuzufuegen, muss man die Umgebungsvariable ``PYTHONPATH`` erweitern.

Packages
--------

Hat man viele Module, will man diese in einer Ordnerstruktur gliedern um Namenskonflikte zu vermeiden. Dazu dienen **Packages**. Ein Package ist ein beliebiger Ordner, welcher eine Datei ``__ini__.py`` enhaelt. Das Package sollte auch in einem Orner von ``sys.path`` liegen. In einem Package kann man Module ablegen und damit importieren::

  >>> import maya.cmds
  >>> maya.cmds.Help()

In diesem Fall ist ``maya`` das Package und ``cmds`` das Modul.
Um nicht immer ``maya.cmds`` schreiben zu muessen, kann man auch folgendes verwenden::

  >>> import maya.cmds as cmds
  >>> cmds.Help()

nach dem Schluesselwort ``as`` folgt ein beliebiger Variablenname.

Packages koennen auch weiter geschachtelt werden.

.. important:: Beim import eines Packages wird der Code von ``__init__.py`` ausgefuehrt. In der Regel ist diese Datei jedoch einfach leer.

From ... import ...
-------------------

Manchmal will man nur einen kleinen Teil aus einem Modul verwenden.
Dabei hilft das Schluesselwort ``from``::

  >>> from maya.cmds import Help
  >>> Help()

.. note:: Das maya.cmds Modul kann man nur innerhalb von Maya verwenden. Will man es au�erhalb verwenden, ist erheblich mehr Aufwand gefordert!
