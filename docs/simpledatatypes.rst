Einfache Datentypen
===================

Zahlen
------

Python kann auch wie ein Taschenrechner benutzt werden::

  >>> 2+2
  4
  >>> # Kommentare, um Code zu erklaeren
  ... 2*5 # ich mutipliziere jetzt
  10
  >>> # Teilen mit ganzen Zahlen ergibt wieder eine ganze Zahl
  ... 7/3
  2
  >>> 7/-3
  -3

Fuer Gleitkommazahlen verwendet man in Python ``.`` - den Punkt::

  >>> 7/3.0
  2.3333333333333335
  >>> 7./3
  2.3333333333333335

Variablen werden mit ``=`` zugewiesen und koennen danach ebenso verwendet werden::

  >>> x = 3
  ... y = 2.0
  ... z = x*y
  ... z
  6.0

Python ist **dynamisch**. Der Typ einer Variable kann sich aendern::

  >>> x = 3  # x ist ein Integer
  ... x = 2.5  # x ist nun eine Gleitkommazahl

Strings
-------

Strings koennen in einfachen oder doppelten Hochkommata stehen::

  >>> "It's just a flesh wound."
  "It's just a flesh wound."
  >>> 'Now, stand aside, worthy advesary!'
  'Now, stand aside, worthy advesary!'
  >>> '\'Tis but a scratch!'
  "'Tis but a scratch"
  >>> "\"We need more double quotes \" \" over here!\""
  '"We need more double quotes " " over here!"'
  >>> """ Triple Quotes?? Yes for
  multi line strings"""
  ' Triple Quotes?? Yes for\nmulti line strings'

Wenn wir strings **printen**, loest Python Escapecharacter auf::

  >>> print "Print me\nNew Lines\n\tand Tabs\nor do not escape this \\n new line!"
  Print me
  New Lines
          and Tabs
  or do not escape this \n new line!

Strings koennen auch mit Operatoren verwendet werden::

  >>> con = 'Better' + 'Together'
  >>> con
  'BetterTogether'
  >>> mul = 'CloneMe' * 3
  >>> mul
  'CloneMeCloneMeCloneMe'

Strings besitzen auch Methoden::

   >>> camel = 'UpAndDown'
   >>> camel.lower()
   'upanddown'
   >>> camel  # Strings are immutable!
   'UpAndDown'
   >>> camel = 'A Fresh Start'
   >>> camel
   'A Fresh Start'

Strings koennen zerlegt werden (**slicing**)::

   >>> row = 'abcdfgh'
   >>> row[2:4]
   'cd'
   >>> row[:6]
   'abcdfg'
   >>> row[-2:]
   'gh'

Strings koennen auch noch 'intelligenter' zusammengesetzt werden. Dazu nutzt man **Formatter**. Ein Formatter faengt mit ``%`` an. Mit Hilfe des Modulo Operators (ebenfalls das Prozentzeichen) koennen dann Variablen an diese Stellen platziert werden::

   >>> nombre = "Name: %s"
   >>> nombre
   "Name: %s"
   >>> nombre % 'Zuber'
   "Name: Zuber"
   >>> fullname = "%s, Vorname: %s" % (nombre, 'David')
   "Name: Zuber, Vorname: David"
   >>> "%s string, %i integer, %f float" % (3, 3, 3)
   '3 string, 3 integer, 3.000000 float'

Es gibt eine ganze Reihe an moeglichen Formattern. Eine Erlaeuterung ist hier zu finden: `String Format Tutorial <https://docs.python.org/2/library/string.html#format-specification-mini-language>`_

Listen
------

Listen fassen mehrere Werte zusammen. Sie aehneln den Arrays aus Java. Allerdings sind sie wesentlich flexibler::

  >>> a = ['foo', 'bar', 200]
  >>> a
  ['foo', 'bar', 200]
  >>> a[0]
  'foo'
  >>> a[1:3]
  ['bar', 200]
  >>> a[0] = 'Wunder'
  >>> a
  ['Wunder', 'bar', 200]
  >>> a[-2:] = []
  >>> a
  ['Wunder']
  >>> a.append('gibts')
  >>> a.append('nicht')
  >>> a
  ['Wunder', 'gibts', 'nicht']
  >>> len(a)
  3
  >>> a[1] = [a[1], "vielleicht"]
  ['Wunder', ['gibts', 'vielleicht'], 'nicht']
  >>> a[0:2] = 'AB'
  >>> a
  ['A', 'B', 'nicht']

Im Gegensatz zu **Listen** sind **Tupel** unveraenderlich. Sie werden mit runden Klammern ``()`` notiert.

Dictionaries
------------

Python kennt den sehr beliebten Datentyp ``dict``, welcher eine Zuweisung von Schluesseln und Werten darstellt::

  >>> age = {'Terry Jones': 72, 'John Cleese': 74,
  ...        'Michael Palin': 70, 'Graham Chapman': 48,
  ...        'Eric Idle': 71, 'Terry Gilliam': 73}
  >>> age['John Cleese']
  74
  >>> age['Graham Chapman'] * 2
  96
  >>> age['David Zuber']
  
  Traceback (most recent call last):
    File "<pyshell#13>", line 1, in <module>
      age['David Zuber']
  KeyError: 'David Zuber'
  >>> age['David Zuber'] = 21
  >>> age['David Zuber']
  21
