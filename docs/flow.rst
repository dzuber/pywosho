Flow Control
============

Python besitzt wie Java Ausdruecke, um den Programmfluss zu steuern.

if statement
------------

In Python verzichtet man auf geschwungene Klammern. Dafuer muss jedoch der Text eingerueckt werden. Per Konvention werden **4 Leerzeichen** verwendet. Man kann aber auch mehr oder weniger benutzen oder Tabs gebrauchen. Bitte benutzt **IMMER** 4 Leerzeichen!

Hier mal ein kleines Beispiel von ``if``, ``else`` und ``elif``::

  >>> x = 42
  >>> if x < 0:
  ...     print 'Negative', x
  ... else:
  ...     print 'Positive', x
  ... if x == 0:
  ...     print 'Zero', x
  ... elif x == 42:
  ...     print 'the answer to life the universe and everything', x
  ... else:
  ...     print 'I don\'t care!'

for statement
-------------

For-Schleifen in Python laufen immer ueber Elemente einer Sequenz und fuehren jedesmal ihren Code aus::

  >>> liste = ['a', 'b', 'c', 'd']
  >>> for c in liste:
  ...     print int(c)

Die Variable ``c`` nimmt bei jedem Durchgang den naechsten Wert in der Liste an. Dann wird sie zu einem Integer umgewandelt und gedruckt.

In Python gibt es auch den sogenannten Boolean-Datentype, welcher entweder ``True`` oder ``False`` ist::

  >>> for c in liste:
  ...     print c=='a'

Python kann auch implizit einige Datentypen in Booleans umwandeln::

  >>> liste = [0 , 1, -2, 3, 'a', '' , 'False', [], [1, 2, 3], None]
  >>> for i in liste:
  ...     if i:
  ...         print i
  >>> for i in liste:
  ...     print bool(i)  # explizit
  >>> for i in liste:
  ...     print i == 'something'
  ...     print i is None

``None`` entspricht dem ``null`` in Java. Der ``is``-Vergleich prueft auf Identitaet, nicht auf Gleichheit. Somit kann etwas gleich sein, aber nicht das selbe.

range Funktion
--------------

Die range-Funktion erzeugt eine Liste ueber die man iterieren kann. Damit aehnelt es dem arithmetischen For-Schleifen von Java::

  >>> for i in range(10):
  ...     print i
  >>> range (2, 5)
  >>> a = ['Follow', 'the', 'white', 'rabbit!']
  >>> for i in range(len(a)):
  ...     print i, a[i]
  >>> for i, word in enumerate(a):
  ...     print i, word

``enumerate`` ist, wie ``range`` auch, eine eingebaute Funktion, welche einem eine effiziente Moeglichkeit bereitstellt.

