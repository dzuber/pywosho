Klassen
=======

Definition
----------

Wie in anderen objektorientierten Sprachen beherrscht Python Klassen und (Mehrfach-)Vererbung.
Eine Klasse wird mit dem Schluesselwort ``class`` definiert::

  >>> class Fire(object):
  ...     def burn(self, fuel):
  ...         print 'Burning %s' % fuel
  >>> flames = Fire()
  >>> flames.burn('Gasoline')
  Burning Gasoline
  >>> print flames
  <__main__.Fire object at 0x0000000036F19320>

``flames`` ist eine **Instanz** der Klasse ``Fire``. Funktionen, welche innerhalb einer Klasse definiert wurden nennt man **Methoden**.
Eine Methode muss immer von einer Instanz aufgerufen werden! Dabei wird ``self`` zur Referenz auf die **Instanz**.

__init__
--------

Man kann Einfluss auf die Erstellung einer Instanz nehmen::

  >>> class Fuel(object):
  ...     def __init__(self, amount, measure, sort):
  ...         self.quantity = amount
  ...         self.unit = measure
  ...         self.kind = sort
  ...         
  ...     def refill(self, amount):
  ...         newquantity = self.quantity + amount
  ...         self.quantity = newquantity
  ...         
  ...     def __str__(self):
  ...         return '%s %s of %s' % (self.quantity, self.unit, self.kind)
  >>> gas = Fuel(1, 'l', 'Petroleum')
  >>> gas.quantity = 40
  >>> gas.refill(2)
  >>> flames.burn(gas)
  Burning 42 l of Petroluem

Hier sieht man 2 spezielle Methoden:

  :__init__: Ist quasi der Konstruktor der Klasse. Bei der Erstellung der Instanz wird der COde in  ``__init__`` ausgefuehrt.
  :__str__: Wird immer automatisch verwendet, wenn eine Instanz als String dargestellt werden soll. Zum Beispiel beim Stingformatieren mit ``%s``, bei ``print`` oder mit ``str(instanz)``

Das erste Argument von Methoden ist die Referenz auf die eigene Instanz. Es ist Konvention es ``self`` zu nennen.

Attribute
---------

Wichtig ist: Ein Attribut kann erst nach der Zuweisung ausgelesen werden. Eine Zuweisung kann jedoch zu jeder Zeit stattfinden::

  >>> class Car(object):
  ...     def __init__(self):
  ...         # Dieses Auto ist noch leer
  ...         pass
  ... 
  ...     def start(self):
  ...         print 'Starting %s' % self.engine
  ... 
  ...     def refuel(self, f):
  ...         self.fuel = f
  ... 
  ...     def drive(self):
  ...         self.start()
  ...         print 'Used %s' % self.fuel
  ... 
  >>> c = Car()
  >>> #c.start() # FAIL! Kein Motor
  >>> c.engine = 'Cadillac V8 L-Head'
  >>> #c.drive() # FAIL! Kein Brennstoff!
  >>> gas = Fuel(5, 'gallons', 'Gas')
  >>> c.refuel(gas) #  fuel wird zugewiesen
  >>> c.drive()
  Starting Cadillac V8 L-Head
  Used 5 gallons of Gas

Vererbung
---------

Bei der Vererbung erhaelt eine erbende Klasse alle Methoden der Elterklasse. Zusaetzlich kann man neue Methoden definieren::

  >>> class Hero(object):
  ...     def __init__(self, superpower):
  ...         self.power = superpower
  ...         print 'A new Hero is born'
  ... 
  ...     def use_power(self):
  ...         print 'USING %s' % self.power

  >>> class TragicHero(Hero):
  ...     def die(self):
  ...         print 'Avenge me!!!'
  ... 
  >>> aristotle = TragicHero('extreme Brainpower')
  A new Hero is born
  >>> aristotle.use_power()
  USING exreme Brainpower
  >>> aristotle.die()
  Avenge me!!!

Es ist auch moeglich eine Methode zu ueberschreiben. Dabei muss sie einfach nocheinmal definiert werden. Man kann jedoch mit einem Trick auf die urspuengliche Methode zugreifen::

  >>> class SuperHero(TragicHero):
  ...     def __init__(self, power, assistant):
  ...         super(SuperHero, self).__init__(power)
  ...         self.sidekick = assistant
  ... 
  ...     def fly(self):
  ...         print 'SWOOOSH'
  ... 
  ...     def die(self):
  ...         print "I'm invincible! Try again!"
 
  >>> superman = SuperHero('Xray Vision', 'Jimmy Olsen')
  A new Hero is born
  >>> superman.die()
  I'm invincible! Try again!
  >>> super(SuperHero, superman).die()
  Avenge me!!!

Der Trick mit ``super`` wird in der Regel nur beim Ueberschreiben von Methoden verwendet. So kann man eine Methode in ihrer Funktionalitaet erweitern.

.. Important:: Beim ueberschreiben der ``__init__``-Methode sollte grundsaetzlich die ``__init__``-Methode der Elterklasse aufgerufen werden. Sonst fehlen zum Beispiel der Kindklasse Attribute.
